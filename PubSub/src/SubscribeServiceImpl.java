/**
 * Created by mingyama on 1/30/18.
 */

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.util.*;

public class SubscribeServiceImpl extends UnicastRemoteObject implements SubscribeService {

    private final int MAX_LENGTH = 120;

    private final int DEFAULT_PORT = 80;

    private String[] TYPES = {"Sports", "LifeStyle", "Entertainment", "Business",
            "Technology", "Science", "Politics", "Health"};

    // the store structure only ensures that the subscribe list are updated,
    // however any publications are not stored in any forms
    // so the subscribers only receive contents from the date they subscribe
    private HashMap<String, Integer> addressBook; //ip -> port
    private HashMap<String, List<String>> typeBook;  //type -> ip
    private HashMap<String, List<String>> originatorBook; // originator -> ip
    private HashMap<String, List<String>> orgBook; // org -> ip

    public SubscribeServiceImpl() throws RemoteException {
        addressBook = new HashMap<>(); //ip -> port
        typeBook = new HashMap<>();
        for (String type : TYPES) {
            typeBook.put(type, new ArrayList<>());
        }
        originatorBook = new HashMap<>();
        orgBook = new HashMap<>();
    }

    @Override
    public String subscribe(String info) throws RemoteException {
        String clientAddress;
        try {
            Article subscribeInfo = new Article(info);
            clientAddress = getClientHost();
            if (subscribeInfo.isValid() && subscribeInfo.contents == null) {
                addressBook.put(clientAddress, DEFAULT_PORT);

                String type = subscribeInfo.type;
                String originator = subscribeInfo.originator;
                String org = subscribeInfo.org;

                if (type != null && !type.equals("")) {
                    if (typeBook.containsKey(type))
                        typeBook.get(type).add(clientAddress);
                    else return "No Such Type to Subscribe!";
                }
                if (originator != null && !originator.equals("")) {
                    if (originatorBook.containsKey(originator)) {
                        originatorBook.put(originator, new ArrayList<>());
                    }
                    originatorBook.get(originator).add(clientAddress);
                }
                if (org != null && !org.equals("")) {
                    if (orgBook.containsKey(org)) {
                        orgBook.put(org, new ArrayList<>());
                    }
                    orgBook.get(org).add(clientAddress);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception:" + e.toString());
            return e.toString();
        }
        return "\nsubscribed!";
    }

    private void removeClient(HashMap<String, List<String>> map, String address) {
        for (String key : map.keySet()) {
            List<String> values = map.get(key);
            for (int i = 0; i < values.size(); i++) {
                String clientAddress = values.get(i);
                if (clientAddress.equals(address)) {
                    values.remove(address);
                }
            }
        }
    }

    @Override
    public String unsubscribe() throws RemoteException {
        String address = "";
        try {
            address = getClientHost();
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
            return "No Service Available";
        }
        if (addressBook.containsKey(address)) {
            removeClient(typeBook, address);
            removeClient(originatorBook, address);
            removeClient(orgBook, address);
        } else {
            return "No Client Found with Address, Please Type Correct Address to Unsubscribe!";
        }
        return "Unsubscribe Successful";
    }

    @Override
    public String register(String ip, int port) throws RemoteException {
        try {
            String clientIp;
            if (ip == null || ip.length() == 0) {
                clientIp = getClientHost();
                addressBook.put(clientIp, port);
            } else {
                addressBook.put(ip, port);
            }
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }

        return "registered!";
    }

    @Override
    public void leave() throws RemoteException {
        try {
            String clientAddress = getClientHost();
            if (addressBook.containsKey(clientAddress)) {
                addressBook.remove(clientAddress);
                return;
            }
//            throw new RemoteException("No Such Client Registered!");
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(String info) throws RemoteException {
        String clientAddress;
        try {
            clientAddress = getClientHost();
            System.out.println(clientAddress);
            if (!addressBook.containsKey(clientAddress)) {
                addressBook.put(clientAddress, DEFAULT_PORT);
            }
            Article publication = new Article(info);
            if (publication.isValid() || publication.isPublication()) {
                broadcast(clientAddress, DEFAULT_PORT, publication.toString());
            }
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void broadcast(String ip, int port, String content) throws RemoteException {
        byte[] buffer = content.getBytes();
        try {
            DatagramSocket client = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(ip), port);
            client.send(packet);
            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receiveContent(String ip, int port, byte[] buffer) throws RemoteException {
        try {
            DatagramSocket server = new DatagramSocket(port);
            DatagramPacket packet = new DatagramPacket(buffer, MAX_LENGTH);
            server.receive(packet);
            server.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void syncInfoToServer(HashMap<String, Integer> address, List<HashMap<String, List<String>>> allBooks) throws RemoteException {
        if (addressBook == null) return;
        allBooks.add(new HashMap<>(typeBook));
        allBooks.add(new HashMap<>(originatorBook));
        allBooks.add(new HashMap<>(orgBook));
        for (String ip : addressBook.keySet()) {
            address.put(ip, addressBook.get(ip));
        }
    }

}
