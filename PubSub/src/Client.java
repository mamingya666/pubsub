/**
 * Created by mingyama on 1/29/18.
 */

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.net.MalformedURLException;
import java.util.Scanner;

public class Client {

    private final static String REGISTERY_SERVER_IP = "128.101.35.147";
    private final static int REGISTERY_SERVER_PORT = 5105;
    private final static String SERVER_NAME = "Service.SubscribeServer";
    private final static int MAX_LENGTH = 128;
    private static byte[] buffer = new byte[MAX_LENGTH];;

    public static class ReceivingThread extends Thread {
        @Override
        public void run() {
            try {
                DatagramSocket server = new DatagramSocket(8000);
                DatagramPacket packet = new DatagramPacket(buffer, MAX_LENGTH, InetAddress.getByName("127.0.0.1"), 8000);
                server.receive(packet);
                String data = new String(buffer);
                System.out.println(data);
                server.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    Thread publishingArticleThread;

    public Client() {
        publishingArticleThread = new Thread();
    }

    public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry("localhost");
        SubscribeService service = (SubscribeService) registry.lookup(SERVER_NAME);
//        String clientAddress = service.register();
        while (true) {
//            service.leave();
//            System.out.println(service.subscribe("Science;;UMN;;"));
//            service.unsubscribe();
            Scanner scanner = new Scanner( System.in );
            String input = scanner.nextLine();
            if (input.length() < 3) {
                System.out.println("Invalid inputs, please try again!");
                return;
            }
            String command = input.substring(0, 3);

            byte[] buffer = new byte[128];
            try {
                DatagramSocket server = new DatagramSocket(8000);
                DatagramPacket packet = new DatagramPacket(buffer, 128);
                server.receive(packet);
//                server.close();
                InetAddress clientAddress = packet.getAddress();
                int port = packet.getPort();
                byte[] buf = new byte[packet.getLength()];
                buf = packet.getData();
                String data = new String(buf);
                System.out.println(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            switch(command) {
                case "sub":
                    String subContent = input.substring(3).trim();
                    String subResponse = service.subscribe(subContent);
                    System.out.println(subResponse);
                    break;

                case "usb":
                    String usbResponse = service.unsubscribe();
                    System.out.println(usbResponse);
                    break;

                case "rgt":
                    String ip = "";
                    if (input.length() > 3) {
                        ip = input.substring(3).trim();
                    }
                    String rgtResponse = service.register(ip, 80);
                    System.out.println(rgtResponse);
                    break;

                case "pub":
                    String pubContent = input.substring(3).trim();
//                    service.publish(pubContent);
                    try {
                        service.publish("Science;;UMN;;");
                        System.out.println("published!");
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("error");
                    }
                    break;

                case "lev":
                    service.leave();
                    break;

                case "qqq":
                    return;

                default:
                    System.out.println("invalid input");
                    break;
            }
            System.out.println( "input = " + input );
            System.out.println( "receiving data...");
            ReceivingThread receivingThread = new ReceivingThread();
            receivingThread.run();
//        String testStr = "Science;;;";
//        String[] strs = testStr.split(";");
//        for (String str : strs) {
//            System.out.println(str);
//        }
//        System.out.println(strs.length);
        }
    }
}
