import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mingyama on 1/29/18.
 */
public class Server {

    private static SubscribeService stub;

    private final static String SERVER_NAME = "Service.SubscribeServer";

    static HashMap<String, Integer> addressBook = new HashMap<>(); //ip -> port
    static HashMap<String, List<String>> typeBook = new HashMap<>();  //type -> ip
    static HashMap<String, List<String>> originatorBook = new HashMap<>(); // originator -> ip
    static HashMap<String, List<String>> orgBook = new HashMap<>(); // org -> ip
    static List<HashMap<String, List<String>>> temporaryContainer = new ArrayList<>();

    private static void syncData(List<HashMap<String, List<String>>> temporaryContainer) {
        assert temporaryContainer.size() == 3;
        typeBook = temporaryContainer.get(0);
        originatorBook = temporaryContainer.get(1);
        orgBook = temporaryContainer.get(2);
    }

    public static void main(String[] args) throws RemoteException, MalformedURLException {
        // create security manager
//        if (System.getSecurityManager() == null) {
//            System.setSecurityManager(new SecurityManager());
//        }
        try {
            LocateRegistry.createRegistry(1099);
            stub = new SubscribeServiceImpl();
            Naming.rebind(SERVER_NAME, stub);
            System.out.println("server.RMI Server is ready.");
            byte[] buffer = "Sports;mingya;UMN;you are a little dick".getBytes();
            try {
                DatagramSocket client = new DatagramSocket();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("127.0.0.1"), 8000);
                client.send(packet);
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        while (true) {
//            stub.syncInfoToServer(addressBook, temporaryContainer);
//            syncData(temporaryContainer);
//        }
    }

}
