/**
 * Created by mingyama on 1/29/18.
 */
public class Article {
    String type;
    String originator;
    String org;
    String contents;
    String receivedString;

    public static int LENGTH = 128;


    public Article(String receivedString) {
        //received string will be formatted as "type;originator;org;contents"
        this.receivedString = receivedString;
        String splitor = ";";
        String[] received = receivedString.split(splitor);
        int len = received.length;
        if (len > 0) {
            this.type = received[0];
        } else if (len > 1) {
            this.originator = received[1];
        } else if (len > 2) {
            this.org = received[2];
        } else if (len > 3) {
            this.contents = received[3];
        }
    }



    public boolean isValid() {
        return (type !=null || type.length() != 0 ) || (originator != null || originator.length() != 0 ) || (org != null || org.length() != 0 );
    }

    public boolean isPublication() {
        return contents != null;
    }

    public String toString() {
        return receivedString;
    }
}
