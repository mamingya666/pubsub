import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mingyama on 1/30/18.
 */
public interface SubscribeService extends Remote {

    String subscribe(String article) throws RemoteException;

    String unsubscribe() throws RemoteException;

    String register(String ip, int port) throws RemoteException;

    void leave() throws RemoteException;

    void publish(String info) throws RemoteException;

    void broadcast(String ip, int port, String content) throws RemoteException;

    void receiveContent(String ip, int port, byte[] buffer) throws RemoteException;

    void syncInfoToServer(HashMap<String, Integer> addressBook, List<HashMap<String, List<String>>> allBooks) throws RemoteException;
}
